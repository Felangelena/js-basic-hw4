"use strict";

let num1 = 0,
    num2 = 0;

while (num1 == '' || num2 == '' || num1 == null || num2 == null || isNaN(num1) || isNaN(num2)) {
    num1 = prompt("Please enter first number", num1);
    num2 = prompt("Please enter second number", num2);
}

let operation = prompt("Please enter one of the sign +, -, *, /", "");

function calculate (num1, num2, operation) {
    let total;
    switch (operation) {
        case '+':
            total = num1 + num2;
            break;
        case '-':
            total = num1 - num2;
            break;
        case '*':
            total = num1 * num2;
            break;
        case '/':
            total = num1 / num2;
            break;
    }
    return total;
}

console.log(calculate(+num1, +num2, operation));